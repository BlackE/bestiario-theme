<?php
        /*      ******************************************                     
                ADMIN FUNCTIONS 
         ******************************************        */

        //MENU AGREGAR AL DASHBOARD
        add_theme_support('menus');
        //identificar el NAVBAR para agregarlo en HEADER.PHP
        register_nav_menu( 'primary', __( 'Primary Menu', 'theme-slug' ) ); 
        //add THUMBAILS
        add_theme_support('category-thumbnails');
        add_theme_support( 'post-thumbnails' );
        
        //Agregar sidebars DEKSTOP Y MOBILE
        function my_custom_sidebar() {
            register_sidebar(
                array (
                    'name' => __( 'Mobile Sidebar', 'bestiario-theme' ),
                    'id' => 'mobile-side-bar',
                    'description' => __( 'Elements show on sidebar on mobile', 'bestiario-theme' ),
                    'before_widget' => '<div class="widget-content">',
                    'after_widget' => "</div>",
                    'before_title' => '<h3 class="widget-title">',
                    'after_title' => '</h3>',
                )
            );
            
            register_sidebar( array(
                'name'          => __( 'Desktop Sidebar', 'bestiario-theme' ),
                'id'            => 'desktop-side-bar',
                'description' => __( 'Elements show on sidebar on desktop', 'bestiario-theme' ),
                'before_widget' => '<aside id="%1$s" class="widget %2$s">',
                'after_widget'  => '</aside>',
                'before_title'  => '<h3 class="widget-title">',
                'after_title'   => '</h3>',
            ) );
        }
        add_action( 'widgets_init', 'my_custom_sidebar' );
        
        
        
        
        
        /* ******************************************             
                front FUNCTIONS 
         ******************************************        */
        function load_stylesheets(){
            wp_register_style('normalize',get_template_directory_uri() . "/css/normalize.css",array(),1,"all");
            wp_enqueue_style('normalize');
            wp_register_style('skeleton',get_template_directory_uri() . "/css/skeleton.css",array(),1,"all");
            wp_enqueue_style('skeleton');
            
            wp_register_script("sidebar","https://cdn.ampproject.org/v0/amp-sidebar-0.1.js",array(),1,"all");
            wp_enqueue_script("sidebar");
            
            if(is_front_page())
            {
                wp_register_style('front-page',get_template_directory_uri() . "/css/index.css",array(),1,"all");
                wp_enqueue_style('front-page');
            }else{
                if(is_page()){
                    wp_register_style('categorie',get_template_directory_uri() . "/css/categorie.css",array(),1,"all");
                    wp_enqueue_style('categorie');
                }
            }
            
            if(is_category()){
                wp_register_style('categorie',get_template_directory_uri() . "/css/categorie.css",array(),1,"all");
                wp_enqueue_style('categorie');
            }
            
            if(is_single()){
                wp_register_style('single',get_template_directory_uri() . "/css/single.css",array(),1,"all");
                wp_enqueue_style('single');
            }
            
            if(is_search() || is_archive()){
                wp_register_style('search_archive',get_template_directory_uri() . "/css/search_archive.css",array(),1,"all");
                wp_enqueue_style('search_archive');
            }
        }
        add_action('wp_enqueue_scripts','load_stylesheets');
        
        
        //load scripts
        function add_script(){
            
            wp_deregister_script('jquery');
            wp_register_script('jquery', get_template_directory_uri() . "/script/jquery-3.4.1.min.js" , array(), 1,1 );
            wp_enqueue_script('jquery');
            
            if( is_front_page() )
            {
                wp_register_script("modal","https://cdn.ampproject.org/v0/amp-lightbox-0.1.js",array(),1,"all");
                wp_enqueue_script("modal");
                wp_register_script("carousel","https://cdn.ampproject.org/v0/amp-carousel-0.1.js",array(),1,"all");
                wp_enqueue_script("carousel");
                wp_register_script('custom', get_template_directory_uri() . "/script/index.js" , array(), 1,1 );
                wp_enqueue_script('custom');
            }
            
            if(is_category()){
                wp_register_script('scroll2Top', get_template_directory_uri() . "/script/scroll2Top.js" , array(), 1,1 );
                wp_enqueue_script('scroll2Top');
            }
            
        }
        add_action('wp_enqueue_scripts','add_script');
        
        
        
        /*          COMMENTS                */
        // function crunchify_move_comment_form_below( $fields ) { 
        //     $comment_field = $fields['comment']; 
        //     unset( $fields['comment'] ); 
        //     $fields['comment'] = '<p class="comment-form-author">
        // 			<textarea id="comment" name="comment" cols="45" rows="8" placeholder="' . esc_attr__( "Dejanos un mensaje", "text-domain" ) . '" maxlength="65525" required="required"></textarea>
        // 		</p>';; 
        //     return $fields; 
        // } 
        // add_filter( 'comment_form_fields', 'crunchify_move_comment_form_below' );

        // function my_update_comment_fields( $fields ) {
        
        // 	$commenter = wp_get_current_commenter();
        // 	$req       = get_option( 'require_name_email' );
        // 	$label     = $req ? '*' : ' ' . __( '(optional)', 'text-domain' );
        // 	$aria_req  = $req ? "aria-required='true'" : '';
        
        //     $fields['author'] =
        // 		'<p class="comment-form-author">
        // 			<input id="author" name="author" type="text" placeholder="' . esc_attr__( "Tu Nombre", "text-domain" ) . '" value="' . esc_attr( $commenter['comment_author'] ) .
        // 		'" size="30" ' . $aria_req . ' />
        // 		</p>';
        
        // 	$fields['email'] =
        // 		'<p class="comment-form-email">
        // 			<input id="email" name="email" type="email" placeholder="' . esc_attr__( "ejemplo@gmail.com", "text-domain" ) . '" value="' . esc_attr( $commenter['comment_author_email'] ) .
        // 		'" size="30" ' . $aria_req . ' />
        // 		</p>';
        		
        //     if(isset($fields['url']))
        //         unset($fields['url']);
        
        // 	return $fields;
        // }
        // add_filter( 'comment_form_default_fields', 'my_update_comment_fields' );
        
        
        function bestiario_comment_form_fields( $args = array(), $post_id = null ) {
            if ( null === $post_id )
                $post_id = get_the_ID();
            else
                $id = $post_id;
        
            $commenter = wp_get_current_commenter();
            $user = wp_get_current_user();
            $user_identity = $user->exists() ? $user->display_name : '';
        
            $args = wp_parse_args( $args );
            if ( ! isset( $args['format'] ) )
                $args['format'] = current_theme_supports( 'html5', 'comment-form' ) ? 'html5' : 'xhtml';
        
            $req      = get_option( 'require_name_email' );
            $aria_req = ( $req ? " aria-required='true'" : '' );
            $html5    = 'html5' === $args['format'];
            $fields   =  array(
                     'author' =>
                '<p class="comment-form-author"><label for="author">' . __( 'Name', 'domainreference' ) . '</label> ' .
                ( $req ? '<span class="required">*</span>' : '' ) .
                '<input id="author" name="author" type="text" value="' . esc_attr( $commenter['comment_author'] ) .
                '" size="30"' . $aria_req . ' /></p>',
            
              'email' =>
                '<p class="comment-form-email"><label for="email">' . __( 'Email', 'domainreference' ) . '</label> ' .
                ( $req ? '<span class="required">*</span>' : '' ) .
                '<input id="email" name="email" type="text" value="' . esc_attr(  $commenter['comment_author_email'] ) .
                '" size="30"' . $aria_req . ' /></p>',
            
              'url' =>
                '<p class="comment-form-url"><label for="url">' . __( 'Website', 'domainreference' ) . '</label>' .
                '<input id="url" name="url" type="text" value="' . esc_attr( $commenter['comment_author_url'] ) .
                '" size="30" /></p>',
            );
        
            //$required_text = sprintf( ' ' . __( 'Required fiels are marked %s', 'pietergoosen' ), '<span class="required">*</span>' );
        
            //$fields = apply_filters( 'comment_form_default_fields', $fields );
            $defaults = array(
                'fields'               => $fields,
                'comment_field'        => '<p class="comment-form-comment"><label for="comment">' . _x( 'Comment', 'Comments field name', 'pietergoosen' ) . '</label> <textarea id="comment" name="comment" placeholder="'.__( '*Required* Enter your comment here. Minimum 20 characters, please', 'pietergoosen' ).'" cols="45" rows="8" aria-required="true"></textarea></p>',
                'must_log_in'          => '<p class="must-log-in">' . sprintf( __( 'You must be <a href="%s">logged in</a> to post a comment.', 'pietergoosen' ), wp_login_url( apply_filters( 'the_permalink', get_permalink( $post_id ) ) ) ) . '</p>',
                'logged_in_as'         => '<p class="logged-in-as">' . sprintf( __( 'Logged in as <a href="%1$s">%2$s</a>. <a href="%3$s" title="Log out of this account">Log out?</a>', 'pietergoosen' ), get_edit_user_link(), $user_identity, wp_logout_url( apply_filters( 'the_permalink', get_permalink( $post_id ) ) ) ) . '</p>',
                'comment_notes_before' => '<p class="comment-notes">' . __( 'Your email address will not be published.', 'pietergoosen' ) . ( $req ? $required_text : '' ) . '</p>',
                // 'comment_notes_after'  => '<p class="form-allowed-tags">' . sprintf( __( 'You may use these <abbr title="HyperText Markup Language">HTML</abbr> tags and attributes: %s', 'pietergoosen' ), ' <code>' . allowed_tags() . '</code>' ) . '</p>',
                'id_form'              => 'commentform',
                'id_submit'            => 'submit',
                'title_reply'          => __( 'Leave a Reply', 'pietergoosen' ),
                'title_reply_to'       => __( 'Leave a Reply to %s', 'pietergoosen' ),
                'cancel_reply_link'    => __( 'Cancel reply', 'pietergoosen' ),
                'label_submit'         => __( 'Post Comment', 'pietergoosen' ),
                'format'               => 'xhtml',
                );
            return $defaults;
        }
        
        //add_filter('comment_form_defaults', 'bestiario_comment_form_fields');
        
        
        
        /*  SINGLE      TAGS*/
        add_filter('term_links-post_tag','limit_to_five_tags');
        function limit_to_five_tags($terms) {
            return array_slice($terms,0,5,true);
        }
        
        
?>