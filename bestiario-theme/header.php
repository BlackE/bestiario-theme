<!doctype html>
<html amp lang="en">
    <head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        
        <meta name="viewport" content="width=device-width,minimum-scale=1,initial-scale=1">
        <title>BESTIARIO</title>
        <?php wp_head();?>
        
        <script async src="https://cdn.ampproject.org/v0.js"></script>
        <link rel="canonical" href="bestiario.amp.html">
        
    </head>
    <body>
        <!-- Start Navbar -->
        <header class="ampstart-headerbar fixed flex justify-start items-center top-0 left-0 right-0 pl2 pr4 ">
          <div class="div_logo">
            <amp-layout layout="container" width="1" height="1" >
                <a href="/">
                  <table>
                    <tr>
                      <td>
                        <svg width="60" height="60" viewBox="0 0 60 60">
                            <use id="" xlink:href="<?php bloginfo('template_directory'); ?>/img/logo.svg#Logo"></use>
                        </svg>
                      </td>
                    </tr>
                  </table>
                </a>
            </amp-layout>
          </div>
          <div class="div_superior">
            <div class="div_flex">
              <div role="button" aria-label="open sidebar" on="tap:sidebar.toggle" tabindex="0" class="ampstart-navbar-trigger  pr2  ">
                  <svg  width="44" height="38" viewBox="0 0 44 33.969">
                      <use id="" xlink:href="<?php bloginfo('template_directory'); ?>/img/menu.svg#menu"></use>
                  </svg>
                  <p>Menu</p>
              </div>
              <div>
                  <a href="http://cientificosanonimos.org/" target="_blank">
                    <svg  width="44" height="38" viewBox="0 0 39 38">
                        <use id="" xlink:href="<?php bloginfo('template_directory'); ?>/img/animal.svg#animal"></use>
                    </svg>
                    <p>SCA</p>
                  </a>
              </div>
            </div>
          </div>
        </header>


        <!-- Start Sidebar -->
        <amp-sidebar id="sidebar" class="ampstart-sidebar px3  gallery-sidebar" layout="nodisplay" side="right" >
          <div class="flex justify-start items-center ampstart-sidebar-header">
            <div role="button" aria-label="close sidebar" on="tap:sidebar.toggle" tabindex="0" class="ampstart-navbar-trigger items-start">✕</div>
          </div>
        
          <nav class="ampstart-sidebar-nav ampstart-nav">
              <?php
                wp_nav_menu(
                    array(
                        'theme_location' => 'primary',
                        'menu_class' => 'list-reset m0 p0 ampstart-label',
                        
                    )
                )
              ?>
            <div class="sidebar_footer">
              <hr>
              <div class="social_sidebar_footer">
                <table>
                  <tr>
                    <td>
                      <p>CONTACTO:</p>
                    </td>
                    <td>
                      <a href="#">
                        <svg width="30" height="30" aria-hidden="true" focusable="false" data-prefix="far" data-icon="envelope" class="svg-inline--fa fa-envelope fa-w-16" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                          <path fill="#7a6540" d="M464 64H48C21.49 64 0 85.49 0 112v288c0 26.51 21.49 48 48 48h416c26.51 0 48-21.49 48-48V112c0-26.51-21.49-48-48-48zm0 48v40.805c-22.422 18.259-58.168 46.651-134.587 106.49-16.841 13.247-50.201 45.072-73.413 44.701-23.208.375-56.579-31.459-73.413-44.701C106.18 199.465 70.425 171.067 48 152.805V112h416zM48 400V214.398c22.914 18.251 55.409 43.862 104.938 82.646 21.857 17.205 60.134 55.186 103.062 54.955 42.717.231 80.509-37.199 103.053-54.947 49.528-38.783 82.032-64.401 104.947-82.653V400H48z"></path></svg>
                      </a>
                    </td>
                    <td>
                      <a href="#">
                        <svg width="40" height="30" aria-hidden="true" focusable="false" data-prefix="fab" data-icon="instagram" class="svg-inline--fa fa-instagram fa-w-14" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><path fill="#7a6540" d="M224.1 141c-63.6 0-114.9 51.3-114.9 114.9s51.3 114.9 114.9 114.9S339 319.5 339 255.9 287.7 141 224.1 141zm0 189.6c-41.1 0-74.7-33.5-74.7-74.7s33.5-74.7 74.7-74.7 74.7 33.5 74.7 74.7-33.6 74.7-74.7 74.7zm146.4-194.3c0 14.9-12 26.8-26.8 26.8-14.9 0-26.8-12-26.8-26.8s12-26.8 26.8-26.8 26.8 12 26.8 26.8zm76.1 27.2c-1.7-35.9-9.9-67.7-36.2-93.9-26.2-26.2-58-34.4-93.9-36.2-37-2.1-147.9-2.1-184.9 0-35.8 1.7-67.6 9.9-93.9 36.1s-34.4 58-36.2 93.9c-2.1 37-2.1 147.9 0 184.9 1.7 35.9 9.9 67.7 36.2 93.9s58 34.4 93.9 36.2c37 2.1 147.9 2.1 184.9 0 35.9-1.7 67.7-9.9 93.9-36.2 26.2-26.2 34.4-58 36.2-93.9 2.1-37 2.1-147.8 0-184.8zM398.8 388c-7.8 19.6-22.9 34.7-42.6 42.6-29.5 11.7-99.5 9-132.1 9s-102.7 2.6-132.1-9c-19.6-7.8-34.7-22.9-42.6-42.6-11.7-29.5-9-99.5-9-132.1s-2.6-102.7 9-132.1c7.8-19.6 22.9-34.7 42.6-42.6 29.5-11.7 99.5-9 132.1-9s102.7-2.6 132.1 9c19.6 7.8 34.7 22.9 42.6 42.6 11.7 29.5 9 99.5 9 132.1s2.7 102.7-9 132.1z"></path></svg>
                      </a>
                    </td>
                  </tr>
                </table>
              </div>
              <div class="text_sidebar_footer">
                <p>SOCIEDAD DE CIENTÍFICOS ANÓNIMOS ®  2019  * SITIO POR: <a href="https://studio-sub.com/" target="_blank">SUB</a></p>
            </div>
            </div>
          </nav>
        </amp-sidebar>
        <!-- End Sidebar -->