<?php get_header('alt'); ?>

<main id="site-content" role="main">
    <div class="container">
	    <?php
        	$archive_title = get_the_archive_title();
        	$archive_subtitle = get_the_archive_description( '<div>', '</div>' ); 
        	
        	if ( ( ! is_home() || is_home() && $show_home_header ) && ( $archive_title || $archive_subtitle ) ) : ?>
    		
    		<header class="archive-header section-inner">
    
    			<?php if ( $archive_title ) : ?>
    				<h1 class="archive-title"><?php echo wp_kses_post( $archive_title ); ?></h1>
    			<?php endif; ?>
    
    			<?php if ( $archive_subtitle ) : ?>
    				<div class="archive-subtitle section-inner thin max-percentage intro-text"><?php echo wp_kses_post( wpautop( $archive_subtitle ) ); ?></div>
    			<?php endif; ?>
    			
    		</header><!-- .archive-header -->
    
    	<?php endif; ?>
    
    	<div class="posts section-inner">
    
    		<?php if ( have_posts() ) :?>
    			<div class="posts-grid ">
    			<div class="row">
    				<?php while ( have_posts() ) : the_post(); ?>
    					<div class="grid-item">
    					    <div class="one-third column">
    					    <?php the_title();?>
    					    <?php //the_post_thumbnail();?>
    					    <?php the_permalink();?>
                            </div>
    					</div><!-- .grid-item -->
    
    				<?php endwhile; ?>
    
    			</div><!-- .posts-grid -->
    
    
    		<?php endif; ?>
    	
    	</div><!-- .posts -->
    </div>
</main><!-- #site-content -->

<?php get_footer(); ?>