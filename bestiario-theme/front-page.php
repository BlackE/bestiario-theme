<?php  get_header(); ?>
<!-- End Navbar -->
<main id="content" class="gallery-base-content fit" role="main">
        <button class="open_modal" on="tap:quote-lb">See Quote</button>
        <amp-lightbox id="quote-lb" layout="nodisplay" animate-in="fade-in">
          <blockquote>
              <img src="<?php bloginfo('template_directory'); ?>/img/SCA-Bestiario-LogoAnimado.gif" />
          </blockquote>
          <button class="close_modal" on="tap:quote-lb.close">x</button>
        </amp-lightbox>
      <h1 class="gallery-landing-title">βestiario De Los Λnimales Reales</h1>
      <h1 class="gallery-landing-title"><b>{</b> Que Parecen Inventados <b>}</b></h1>

      <!--<amp-carousel class="carousel" layout="fixed-height"  height="70vh" type="carousel" controls >-->
      <amp-carousel class="carousel" layout="responsive"  height="70vh" type="carousel" controls >
        <div class="carousel_container">
            
            
          <div class="carousel_container_img" id="floresta">
                <amp-layout layout="container" width="1" height="1" media="(min-width: 470px)">
                    <svg id="floresta_" width="560" height="480" viewBox="0 0 730 630">
                        <use id="floresta_mariposaurio" xlink:href="<?php bloginfo('template_directory'); ?>/img/floresta2x.svg#MARIPOSAURIO"></use>
                        <use id="" xlink:href="<?php bloginfo('template_directory'); ?>/img/floresta2x.svg#pexels-photo-2677831"></use>
                        <use id="" xlink:href="<?php bloginfo('template_directory'); ?>/img/floresta2x.svg#e6e4d48aff087bbc349ef50799ee8aed_copy"></use>
                        <use id="floresta_flowerback" xlink:href="<?php bloginfo('template_directory'); ?>/img/floresta2x.svg#flowers-background-butterflies-beautiful-87452"></use>
                        <use id="floresta_historiaNatural" xlink:href="<?php bloginfo('template_directory'); ?>/img/floresta2x.svg#historianatural00roys_0011"></use>
                        <use id="" xlink:href="<?php bloginfo('template_directory'); ?>/img/floresta2x.svg#imagen_cls4"></use>
                        <use id="floresta_pezconcasco" xlink:href="<?php bloginfo('template_directory'); ?>/img/floresta2x.svg#PEZ_CON_CASCO"></use>
                        <use id="" xlink:href="<?php bloginfo('template_directory'); ?>/img/floresta2x.svg#GUSANIN"></use>
                        <use id="" xlink:href="<?php bloginfo('template_directory'); ?>/img/floresta2x.svg#PLANTARPIENTE"></use>
                        <use id="" xlink:href="<?php bloginfo('template_directory'); ?>/img/floresta2x.svg#Metamorphosisin00Meri_0030"></use>
                        <use id="" xlink:href="<?php bloginfo('template_directory'); ?>/img/floresta2x.svg#imagen_cls7"></use>
                        <use id="" xlink:href="<?php bloginfo('template_directory'); ?>/img/floresta2x.svg#Abbildungenneue00Schl_0011_copy"></use>
                        <use id="" xlink:href="<?php bloginfo('template_directory'); ?>/img/floresta2x.svg#image8"></use>
                        <use id="" xlink:href="<?php bloginfo('template_directory'); ?>/img/floresta2x.svg#LAGARTIJA"></use>
                        <use id="" xlink:href="<?php bloginfo('template_directory'); ?>/img/floresta2x.svg#Abbildungenneue00Schl_0105"></use>
                        <use id="" xlink:href="<?php bloginfo('template_directory'); ?>/img/floresta2x.svg#patas_rana"></use>
                        <use id="" xlink:href="<?php bloginfo('template_directory'); ?>/img/floresta2x.svg#historianatural00roys_0011_copy_2"></use>
                    </svg>
                </amp-layout>
                <amp-layout layout="container" media="(max-width: 469px)" width="1" height="1">
                    <svg id="floresta_" width="250" height="211"  viewBox="0 0 730 630">
                        <use id="floresta_mariposaurio" xlink:href="<?php bloginfo('template_directory'); ?>/img/floresta.svg#MARIPOSAURIO"></use>
                        <use id="" xlink:href="<?php bloginfo('template_directory'); ?>/img/floresta.svg#pexels-photo-2677831"></use>
                        <use id="" xlink:href="<?php bloginfo('template_directory'); ?>/img/floresta.svg#e6e4d48aff087bbc349ef50799ee8aed_copy"></use>
                        <use id="floresta_flowerback" xlink:href="<?php bloginfo('template_directory'); ?>/img/floresta.svg#flowers-background-butterflies-beautiful-87452"></use>
                        <use id="floresta_historiaNatural" xlink:href="<?php bloginfo('template_directory'); ?>/img/floresta.svg#historianatural00roys_0011"></use>
                        <use id="" xlink:href="<?php bloginfo('template_directory'); ?>/img/floresta.svg#imagen_cls4"></use>
                        <use id="floresta_pezconcasco" xlink:href="<?php bloginfo('template_directory'); ?>/img/floresta.svg#PEZ_CON_CASCO"></use>
                        <use id="" xlink:href="<?php bloginfo('template_directory'); ?>/img/floresta.svg#GUSANIN"></use>
                        <use id="" xlink:href="<?php bloginfo('template_directory'); ?>/img/floresta.svg#PLANTARPIENTE"></use>
                        <use id="" xlink:href="<?php bloginfo('template_directory'); ?>/img/floresta.svg#Metamorphosisin00Meri_0030"></use>
                        <use id="" xlink:href="<?php bloginfo('template_directory'); ?>/img/floresta.svg#imagen_cls7"></use>
                        <use id="" xlink:href="<?php bloginfo('template_directory'); ?>/img/floresta.svg#Abbildungenneue00Schl_0011_copy"></use>
                        <use id="" xlink:href="<?php bloginfo('template_directory'); ?>/img/floresta.svg#image8"></use>
                        <use id="" xlink:href="<?php bloginfo('template_directory'); ?>/img/floresta.svg#LAGARTIJA"></use>
                        <use id="" xlink:href="<?php bloginfo('template_directory'); ?>/img/floresta.svg#Abbildungenneue00Schl_0105"></use>
                        <use id="" xlink:href="<?php bloginfo('template_directory'); ?>/img/floresta.svg#patas_rana"></use>
                        <use id="" xlink:href="<?php bloginfo('template_directory'); ?>/img/floresta.svg#historianatural00roys_0011_copy_2"></use>
                    </svg>
                </amp-layout>
              <figcaption id="floresta_caption" class="carousel_container_img_caption">
                <h3 role="author">Floresta</h3>
                <a href="<?php echo get_home_url(); ?>/category/floresta/">Ver</a>
              </figcaption>
          </div>
          
          
          
          
          <div class="carousel_container_img" id="pantano">
                <amp-layout layout="container" width="1" height="1" media="(min-width: 470px)">
                    <svg id="pantano_" width="700" height="525" viewBox="0 0 1250 950">
                        <use id="pantano_mariposa" xlink:href="<?php bloginfo('template_directory'); ?>/img/pantano2x.svg#IMGS_4"></use>
                        <use id="" xlink:href="<?php bloginfo('template_directory'); ?>/img/pantano2x.svg#IMGS_2"></use>
                        <use id="pantano_lagartijacasco" xlink:href="<?php bloginfo('template_directory'); ?>/img/pantano2x.svg#lagartija_casco"></use>
                        <use id="" xlink:href="<?php bloginfo('template_directory'); ?>/img/pantano2x.svg#rana_cresta"></use>
                        <use id="" xlink:href="<?php bloginfo('template_directory'); ?>/img/pantano2x.svg#cl1"></use>
                        <use id="pantano_plantarana" xlink:href="<?php bloginfo('template_directory'); ?>/img/pantano2x.svg#plantarana"></use>
                        <use id="" xlink:href="<?php bloginfo('template_directory'); ?>/img/pantano2x.svg#terreno"></use>
                        <use id="pantano_plantaposa" xlink:href="<?php bloginfo('template_directory'); ?>/img/pantano2x.svg#plantaposa"></use>
                        <use id="pantano_ranagrande" xlink:href="<?php bloginfo('template_directory'); ?>/img/pantano2x.svg#rana_grande"></use>
                        <use id="pantano_ranitaarriba" xlink:href="<?php bloginfo('template_directory'); ?>/img/pantano2x.svg#ranita_arriba"></use>
                        <use id="pantano_cabezacocodrilo" xlink:href="<?php bloginfo('template_directory'); ?>/img/pantano2x.svg#cabeza_cocodrilo"></use>
                    </svg>
                </amp-layout>
                <amp-layout layout="container" media="(max-width: 469px)" width="1" height="1">
                    <svg id="pantano_" width="250" height="190" viewBox="0 0 1250 950">
                        <use id="pantano_mariposa" xlink:href="<?php bloginfo('template_directory'); ?>/img/pantano.svg#IMGS_4"></use>
                        <use id="" xlink:href="<?php bloginfo('template_directory'); ?>/img/pantano.svg#IMGS_2"></use>
                        <use id="pantano_lagartijacasco" xlink:href="<?php bloginfo('template_directory'); ?>/img/pantano.svg#lagartija_casco"></use>
                        <use id="" xlink:href="<?php bloginfo('template_directory'); ?>/img/pantano.svg#rana_cresta"></use>
                        <use id="" xlink:href="<?php bloginfo('template_directory'); ?>/img/pantano.svg#cl1"></use>
                        <use id="pantano_plantarana" xlink:href="<?php bloginfo('template_directory'); ?>/img/pantano.svg#plantarana"></use>
                        <use id="" xlink:href="<?php bloginfo('template_directory'); ?>/img/pantano.svg#terreno"></use>
                        <use id="" xlink:href="<?php bloginfo('template_directory'); ?>/img/pantano.svg#plantaposa"></use>
                        <use id="pantano_ranagrande" xlink:href="<?php bloginfo('template_directory'); ?>/img/pantano.svg#rana_grande"></use>
                        <use id="pantano_ranitaarriba" xlink:href="<?php bloginfo('template_directory'); ?>/img/pantano.svg#ranita_arriba"></use>
                        <use id="" xlink:href="<?php bloginfo('template_directory'); ?>/img/pantano.svg#cabeza_cocodrilo"></use>
                    </svg>
                </amp-layout>
              
              
              
              <figcaption id="pantano_caption" class="carousel_container_img_caption">
                <h3 role="author">Meandros y Humedales</h3>
                <a href="<?php echo get_home_url(); ?>/category/meandros-y-humedales/">Ver</a>
              </figcaption>
          </div>

          <div class="carousel_container_img" id="profundidades">
              <amp-layout layout="container" width="1" height="1" media="(min-width: 470px)">
                <svg id="profundidades_" width="550" height="400" viewBox="0 0 1284 900">
                  <use id="" xlink:href="<?php bloginfo('template_directory'); ?>/img/profundidades@2x.svg#calamar1"></use>
                  <use id="" xlink:href="<?php bloginfo('template_directory'); ?>/img/profundidades@2x.svg#calamar2"></use>
                  <use id="" xlink:href="<?php bloginfo('template_directory'); ?>/img/profundidades@2x.svg#calamar3"></use>
                  <use id="profundidades_pez" xlink:href="<?php bloginfo('template_directory'); ?>/img/profundidades@2x.svg#pez"></use>
                  <use id="" xlink:href="<?php bloginfo('template_directory'); ?>/img/profundidades@2x.svg#planta"></use>
                  <use id="" xlink:href="<?php bloginfo('template_directory'); ?>/img/profundidades@2x.svg#manta3"></use>
                  <use id="" xlink:href="<?php bloginfo('template_directory'); ?>/img/profundidades@2x.svg#manta2"></use>
                  <use id="" xlink:href="<?php bloginfo('template_directory'); ?>/img/profundidades@2x.svg#manta1"></use>
                  <use id="" xlink:href="<?php bloginfo('template_directory'); ?>/img/profundidades@2x.svg#pulpo1"></use>
                  <use id="profundidades_pulpo2" xlink:href="<?php bloginfo('template_directory'); ?>/img/profundidades@2x.svg#pulpo2"></use>
                  <use id="" xlink:href="<?php bloginfo('template_directory'); ?>/img/profundidades@2x.svg#buzo"></use>
                  <use id="" xlink:href="<?php bloginfo('template_directory'); ?>/img/profundidades@2x.svg#pezfeo1"></use>
                  <use id="" xlink:href="<?php bloginfo('template_directory'); ?>/img/profundidades@2x.svg#pezfeo2"></use>
                  <use id="" xlink:href="<?php bloginfo('template_directory'); ?>/img/profundidades@2x.svg#sombra_fondo"></use>
                  <use id="" xlink:href="<?php bloginfo('template_directory'); ?>/img/profundidades@2x.svg#planta1"></use>
                  <use id="profundidades_planta2" xlink:href="<?php bloginfo('template_directory'); ?>/img/profundidades@2x.svg#planta2"></use>
                  <use id="profundidades_planta3" xlink:href="<?php bloginfo('template_directory'); ?>/img/profundidades@2x.svg#planta3"></use>
                  <use id="" xlink:href="<?php bloginfo('template_directory'); ?>/img/profundidades@2x.svg#sombra_medusa"></use>
                  <use id="profundidades_medusa" xlink:href="<?php bloginfo('template_directory'); ?>/img/profundidades@2x.svg#medusa"></use>
                </svg>
                </amp-layout>
                <amp-layout layout="container" media="(max-width: 469px)" width="1" height="1">
                  <svg id="profundidades_" width="250" height="185" viewBox="0 0 1284 900">
                    <use id="" xlink:href="<?php bloginfo('template_directory'); ?>/img/profundidades.svg#calamar1"></use>
                    <use id="" xlink:href="<?php bloginfo('template_directory'); ?>/img/profundidades.svg#calamar2"></use>
                    <use id="" xlink:href="<?php bloginfo('template_directory'); ?>/img/profundidades.svg#calamar3"></use>
                    <use id="profundidades_pez" xlink:href="<?php bloginfo('template_directory'); ?>/img/profundidades.svg#pez"></use>
                    <use id="" xlink:href="<?php bloginfo('template_directory'); ?>/img/profundidades.svg#planta"></use>
                    <use id="" xlink:href="<?php bloginfo('template_directory'); ?>/img/profundidades.svg#manta3"></use>
                    <use id="" xlink:href="<?php bloginfo('template_directory'); ?>/img/profundidades.svg#manta2"></use>
                    <use id="" xlink:href="<?php bloginfo('template_directory'); ?>/img/profundidades.svg#manta1"></use>
                    <use id="" xlink:href="<?php bloginfo('template_directory'); ?>/img/profundidades.svg#pulpo1"></use>
                    <use id="profundidades_pulpo2" xlink:href="<?php bloginfo('template_directory'); ?>/img/profundidades.svg#pulpo2"></use>
                    <use id="" xlink:href="<?php bloginfo('template_directory'); ?>/img/profundidades.svg#buzo"></use>
                    <use id="" xlink:href="<?php bloginfo('template_directory'); ?>/img/profundidades.svg#pezfeo1"></use>
                    <use id="" xlink:href="<?php bloginfo('template_directory'); ?>/img/profundidades.svg#pezfeo2"></use>
                    <use id="" xlink:href="<?php bloginfo('template_directory'); ?>/img/profundidades.svg#sombra_fondo"></use>
                    <use id="" xlink:href="<?php bloginfo('template_directory'); ?>/img/profundidades.svg#planta1"></use>
                    <use id="profundidades_planta2" xlink:href="<?php bloginfo('template_directory'); ?>/img/profundidades.svg#planta2"></use>
                    <use id="profundidades_planta3" xlink:href="<?php bloginfo('template_directory'); ?>/img/profundidades.svg#planta3"></use>
                    <use id="" xlink:href="<?php bloginfo('template_directory'); ?>/img/profundidades.svg#sombra_medusa"></use>
                    <use id="profundidades_medusa" xlink:href="<?php bloginfo('template_directory'); ?>/img/profundidades.svg#medusa"></use>
                  </svg>
                </amp-layout>

              <figcaption id="profundidades_caption" class="carousel_container_img_caption">
                <h3 role="author">Profundidades</h3>
                <a href="<?php echo get_home_url(); ?>/category/profundidades/">Ver</a>
              </figcaption>
          </div>

          <div class="carousel_container_img" id="entranas">
            <amp-layout layout="container" width="1" height="1" media="(min-width: 470px)">
              <svg id="entrañas" viewBox="0 0 1387 728"  width="601" height="317" >
                  <use id="entranas_tentaculoAni" xlink:href="<?php bloginfo('template_directory'); ?>/img/entranas@2x.svg#Capa_0_copia_2"></use>
                  <use id="" xlink:href="<?php bloginfo('template_directory'); ?>/img/entranas@2x.svg#Capa_0_copia_copy"></use>
                  <use id="" xlink:href="<?php bloginfo('template_directory'); ?>/img/entranas@2x.svg#Capa_0"></use>
                  <use id="" xlink:href="<?php bloginfo('template_directory'); ?>/img/entranas@2x.svg#Capa_1"></use>
                  <use id="entranas_circuloRotate" xlink:href="<?php bloginfo('template_directory'); ?>/img/entranas@2x.svg#Capa_0_copia_2-2"></use>
                  <use id="entranas_circuloScale" xlink:href="<?php bloginfo('template_directory'); ?>/img/entranas@2x.svg#Capa_0_copia"></use>
                  <use id="" xlink:href="<?php bloginfo('template_directory'); ?>/img/entranas@2x.svg#microscopic-1487444_1280"></use>
                  <use id="entranas_tentaculoRotate" xlink:href="<?php bloginfo('template_directory'); ?>/img/entranas@2x.svg#Capa_0_copia-2"></use>
                  <use id="" xlink:href="<?php bloginfo('template_directory'); ?>/img/entranas@2x.svg#Capa_0_copia_2-3"></use>
                  <use id="" xlink:href="<?php bloginfo('template_directory'); ?>/img/entranas@2x.svg#Capa_0_copia_3"></use>
              </svg>
            </amp-layout>
            <amp-layout layout="container" width="1" height="1" media="(max-width: 469px)">
              <svg id="entrañas" width="249.937" height="131.186" viewBox="0 0 1387 728" >
                  <use id="entranas_tentaculoAni" xlink:href="<?php bloginfo('template_directory'); ?>/img/entranas.svg#Capa_0_copia_2"></use>
                  <use id="" xlink:href="<?php bloginfo('template_directory'); ?>/img/entranas.svg#Capa_0_copia_copy"></use>
                  <use id="" xlink:href="<?php bloginfo('template_directory'); ?>/img/entranas.svg#Capa_0"></use>
                  <use id="" xlink:href="<?php bloginfo('template_directory'); ?>/img/entranas.svg#Capa_1"></use>
                  <use id="entranas_circuloRotate" xlink:href="<?php bloginfo('template_directory'); ?>/img/entranas.svg#Capa_0_copia_2-2"></use>
                  <use id="entranas_circuloScale" xlink:href="<?php bloginfo('template_directory'); ?>/img/entranas.svg#Capa_0_copia"></use>
                  <use id="" xlink:href="<?php bloginfo('template_directory'); ?>/img/entranas.svg#microscopic-1487444_1280"></use>
                  <use id="entranas_tentaculoRotate" xlink:href="<?php bloginfo('template_directory'); ?>/img/entranas.svg#Capa_0_copia-2"></use>
                  <use id="" xlink:href="<?php bloginfo('template_directory'); ?>/img/entranas.svg#Capa_0_copia_2-3"></use>
                  <use id="" xlink:href="<?php bloginfo('template_directory'); ?>/img/entranas.svg#Capa_0_copia_3"></use>
              </svg>
            </amp-layout>
              <figcaption id="entranas_caption" class="carousel_container_img_caption">
                <h3 role="author">Entrañas</h3>
                <a href="<?php echo get_home_url(); ?>/category/entranas/">Ver</a>
              </figcaption>
          </div>

          <div class="carousel_container_img" id="bitacora">
              <amp-layout layout="container" width="1" height="1" media="(min-width: 470px)">
                <svg id="bitacora" width="494" height="309" viewBox="0 0 1040 683" >
                    <use id="" xlink:href="<?php bloginfo('template_directory'); ?>/img/bitacora@2x.svg#planta_fondo"></use>
                    <use id="" xlink:href="<?php bloginfo('template_directory'); ?>/img/bitacora@2x.svg#planta_arriba"></use>
                    <use id="" xlink:href="<?php bloginfo('template_directory'); ?>/img/bitacora@2x.svg#sombra_rana1"></use>
                    <use id="" xlink:href="<?php bloginfo('template_directory'); ?>/img/bitacora@2x.svg#sombra_rana2"></use>
                    <use id="" xlink:href="<?php bloginfo('template_directory'); ?>/img/bitacora@2x.svg#fondo"></use>
                    <use id="" xlink:href="<?php bloginfo('template_directory'); ?>/img/bitacora@2x.svg#rana"></use>
                    <use id="" xlink:href="<?php bloginfo('template_directory'); ?>/img/bitacora@2x.svg#libro"></use>
                    <use id="" xlink:href="<?php bloginfo('template_directory'); ?>/img/bitacora@2x.svg#sombra_dude1"></use>
                    <use id="" xlink:href="<?php bloginfo('template_directory'); ?>/img/bitacora@2x.svg#sombra_dude2"></use>
                    <use id="bitacora_flauta0" xlink:href="<?php bloginfo('template_directory'); ?>/img/bitacora@2x.svg#pluma1"></use>
                    <use id="bitacora_tijeras" xlink:href="<?php bloginfo('template_directory'); ?>/img/bitacora@2x.svg#tijeras"></use>
                    <use id="" xlink:href="<?php bloginfo('template_directory'); ?>/img/bitacora@2x.svg#cuchillo"></use>
                    <use id="bitacora_manoPluma" xlink:href="<?php bloginfo('template_directory'); ?>/img/bitacora@2x.svg#mano_pluma"></use>
                    <use id="" xlink:href="<?php bloginfo('template_directory'); ?>/img/bitacora@2x.svg#tinta"></use>
                    <use id="" xlink:href="<?php bloginfo('template_directory'); ?>/img/bitacora@2x.svg#planta_libelula"></use>
                    <use id="bitacora_flauta1" xlink:href="<?php bloginfo('template_directory'); ?>/img/bitacora@2x.svg#pluma3"></use>
                    <use id="bitacora_flauta2" xlink:href="<?php bloginfo('template_directory'); ?>/img/bitacora@2x.svg#pluma4"></use>
                    <use id="bitacora_flauta3" xlink:href="<?php bloginfo('template_directory'); ?>/img/bitacora@2x.svg#pluma5"></use>
                    <use id="" xlink:href="<?php bloginfo('template_directory'); ?>/img/bitacora@2x.svg#camara_cabeza"></use>
                    <use id="" xlink:href="<?php bloginfo('template_directory'); ?>/img/bitacora@2x.svg#camara_cabeza2"></use>
                    <use id="" xlink:href="<?php bloginfo('template_directory'); ?>/img/bitacora@2x.svg#dude"></use>
                    <use id="" xlink:href="<?php bloginfo('template_directory'); ?>/img/bitacora@2x.svg#cara_dude"></use>
                    <use id="" xlink:href="<?php bloginfo('template_directory'); ?>/img/bitacora@2x.svg#sombra_camisa"></use>
                    <use id="" xlink:href="<?php bloginfo('template_directory'); ?>/img/bitacora@2x.svg#tentaculos_mano"></use>
                    <use id="" xlink:href="<?php bloginfo('template_directory'); ?>/img/bitacora@2x.svg#sombra"></use>
                    <use id="" xlink:href="<?php bloginfo('template_directory'); ?>/img/bitacora@2x.svg#sombra2"></use>
                    <use id="" xlink:href="<?php bloginfo('template_directory'); ?>/img/bitacora@2x.svg#sombra_medusa"></use>
                    <use id="bitacora_ala" xlink:href="<?php bloginfo('template_directory'); ?>/img/bitacora@2x.svg#ala"></use>
                </svg>
              </amp-layout>
              <amp-layout layout="container" width="1" height="1" media="(max-width: 469px)">
                <svg id="bitacora" width="250" height="156" viewBox="0 0 1040 683">
                    <use id="" xlink:href="<?php bloginfo('template_directory'); ?>/img/bitacora.svg#planta_fondo"></use>
                    <use id="" xlink:href="<?php bloginfo('template_directory'); ?>/img/bitacora.svg#planta_arriba"></use>
                    <use id="" xlink:href="<?php bloginfo('template_directory'); ?>/img/bitacora.svg#sombra_rana1"></use>
                    <use id="" xlink:href="<?php bloginfo('template_directory'); ?>/img/bitacora.svg#sombra_rana2"></use>
                    <use id="" xlink:href="<?php bloginfo('template_directory'); ?>/img/bitacora.svg#fondo"></use>
                    <use id="" xlink:href="<?php bloginfo('template_directory'); ?>/img/bitacora.svg#rana"></use>
                    <use id="" xlink:href="<?php bloginfo('template_directory'); ?>/img/bitacora.svg#libro"></use>
                    <use id="" xlink:href="<?php bloginfo('template_directory'); ?>/img/bitacora.svg#sombra_dude1"></use>
                    <use id="" xlink:href="<?php bloginfo('template_directory'); ?>/img/bitacora.svg#sombra_dude2"></use>
                    <use id="bitacora_flauta0" xlink:href="<?php bloginfo('template_directory'); ?>/img/bitacora.svg#pluma1"></use>
                    <use id="bitacora_tijeras" xlink:href="<?php bloginfo('template_directory'); ?>/img/bitacora.svg#tijeras"></use>
                    <use id="" xlink:href="<?php bloginfo('template_directory'); ?>/img/bitacora.svg#cuchillo"></use>
                    <use id="bitacora_manoPluma" xlink:href="<?php bloginfo('template_directory'); ?>/img/bitacora.svg#mano_pluma"></use>
                    <use id="" xlink:href="<?php bloginfo('template_directory'); ?>/img/bitacora.svg#tinta"></use>
                    <use id="" xlink:href="<?php bloginfo('template_directory'); ?>/img/bitacora.svg#planta_libelula"></use>
                    <use id="bitacora_flauta1" xlink:href="<?php bloginfo('template_directory'); ?>/img/bitacora.svg#pluma3"></use>
                    <use id="bitacora_flauta2" xlink:href="<?php bloginfo('template_directory'); ?>/img/bitacora.svg#pluma4"></use>
                    <use id="bitacora_flauta3" xlink:href="<?php bloginfo('template_directory'); ?>/img/bitacora.svg#pluma5"></use>
                    <use id="" xlink:href="<?php bloginfo('template_directory'); ?>/img/bitacora.svg#camara_cabeza"></use>
                    <use id="" xlink:href="<?php bloginfo('template_directory'); ?>/img/bitacora.svg#camara_cabeza2"></use>
                    <use id="" xlink:href="<?php bloginfo('template_directory'); ?>/img/bitacora.svg#dude"></use>
                    <use id="" xlink:href="<?php bloginfo('template_directory'); ?>/img/bitacora.svg#cara_dude"></use>
                    <use id="" xlink:href="<?php bloginfo('template_directory'); ?>/img/bitacora.svg#sombra_camisa"></use>
                    <use id="" xlink:href="<?php bloginfo('template_directory'); ?>/img/bitacora.svg#tentaculos_mano"></use>
                    <use id="" xlink:href="<?php bloginfo('template_directory'); ?>/img/bitacora.svg#sombra"></use>
                    <use id="" xlink:href="<?php bloginfo('template_directory'); ?>/img/bitacora.svg#sombra2"></use>
                    <use id="" xlink:href="<?php bloginfo('template_directory'); ?>/img/bitacora.svg#sombra_medusa"></use>
                    <use id="bitacora_ala" xlink:href="<?php bloginfo('template_directory'); ?>/img/bitacora.svg#ala"></use>
                </svg>
              </amp-layout>
              <figcaption id="bitacora_caption" class="carousel_container_img_caption">
                <h3 role="author">Bitacora</h3>
                <a href="<?php echo get_home_url(); ?>/category/bitacora-de-campo/">Ver</a>
              </figcaption>
          </div>
        </div>
      </amp-carousel>
        <?php $Home = get_field('home');?>
      <div class="button_container">
          <div><a href="<?php echo $Home['link_proyecto'];?>"><button class="btn btn_about"><?php echo $Home['boton_izquierda'];?></button></a></div>
          <div><a href="<?php echo $Home['link_libro'];?>" target="_blank"><button class="btn btn_libro"><?php echo $Home['boton_derecha'];?></button></a></div>
      </div>
</main>

<?php  get_footer(); ?>