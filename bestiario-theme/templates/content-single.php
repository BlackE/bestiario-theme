
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    	<div class="entry-header">
    		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
    	</div><!-- .entry-header -->
    	<div class="author_container">
		    <p>Por: <?php the_author_posts_link();?></p>
		</div>
		<div class="category_container">
		    <p>En:<?php the_category(', '); ?></p>
		</div>
    <div class="row" id="thumbnail">
        <!--<div class="three columns">-->
            <?php if(has_post_thumbnail()){ 
                    $id_thumb = get_post_thumbnail_id($post->ID);
                    $attachment = wp_get_attachment_image_src( $id_thumb ,'full' );
                    $width_thumb=$attachment[1];
                    $height_thumb=$attachment[2];
                    ?>
                <amp-img layout="responsive"
                  class="image"
                  width="<?php echo $width_thumb;?>"
                  height="<?php echo $height_thumb;?>"
                  src="<?php echo get_the_post_thumbnail_url($post->ID);?>"</amp-img>
            <?php }else{?>
                <amp-img layout="responsive"
                  class="image"
                  width="290"
                  height="160"
                  src="https://studio-sub.com/clientes/bestiario_wordpress/wp-content/uploads/2020/01/no_thumb.jpg"</amp-img>
            <?php }?>
        <!--</div>-->
    </div>
    
    	<div class="entry-content">
    		<?php
    		    echo the_content();
    		?>
    	</div><!-- .entry-content -->
    	<div class="entry-footer">
    		
    	</div><!-- .entry-footer -->
</article><!-- #post-<?php the_ID(); ?> -->
