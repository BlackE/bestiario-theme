<?php
    /**
    * BESTIARIO CATEGORY
    */
    get_header("alt");
    $categories = get_the_category();
    $category_id = $categories[0]->cat_ID;
    $array_exclude_post = array();
?>
    <main id="content" class="gallery-base-content fit" role="main">
        <amp-img alt="Categoria_"
            src="
            <?php
                if (function_exists('ttw_thumbnail_url')){
                    echo ttw_thumbnail_url();
                }
            ?>"      
            width="1280"
            height="400"
            layout="responsive">
        </amp-img>
        <hr class="breaker">
        <div class="container">
            <section class="breweries" id="especies_destacadas_container">
              <?php 
                $posts_banner = get_posts( array(
                    'category'         => $category_id,
                    'meta_query' => array(
                        array(
                            'post_type'			=> 'post',
                            'key'   => 'show_in_banner',
                            'value' => '1',
                        )
                    )
                ) );
                
                if( $posts_banner ) {
                    echo '<ul>';
                    foreach( $posts_banner as $post ) {
                        if(has_post_thumbnail($post->ID)){
                            echo '<li>';
                            echo '<a href="'.get_permalink($post->ID).'">';
                            echo '<div class="grid_especie">';
                            echo '<amp-img alt="Categoria_thumb" src="'.get_the_post_thumbnail_url($post->ID).'" width="191" height="191" layout="responsive"> </amp-img>';
                            echo '<p>'.get_the_title($post->ID).'</p>';
                            echo '</div>';
                            echo '</a>';
                            echo '</li>';
                        }
                        else{
                            echo '<li>';
                            echo '<a href="'.get_permalink($post->ID).'">';
                            echo '<div class="grid_especie">';
                            echo '<amp-img alt="Categoria_thumb" src="https://studio-sub.com/clientes/bestiario_wordpress/wp-content/uploads/2020/01/no_thumb.jpg" width="289" height="159" layout="responsive"> </amp-img>';
                            echo '<p>'.get_the_title($post->ID).'</p>';
                            echo '</div>';
                            echo '</a>';
                            echo '</li>';
                        }
                        array_push($array_exclude_post,$post->ID);
                    }
                    echo '</ul>';
                }
              ?>
            </section>
        </div>
        <hr class="breaker">
        <div class="other_species_container">
            <div class="container">
              <p>Conoce otras especies</p>
              <ul>
                  <?php
                    $posts_otras_especies = get_posts( array(
                        'category'         => $category_id,
                        'meta_query' => array(
                            array(
                                'key'   => 'show_in_otras_especies',
                                'value' => '1',
                            )
                        )
                    ) );
                    if( $posts_otras_especies ) {
                        foreach( $posts_otras_especies as $post ) {
                            if(has_category($category_id)){
                                echo '<li>';
                                echo '<a href="'.get_permalink($post->ID).'">';
                                echo get_the_title($post->ID);
                                echo '</a>';
                                echo '</li>';
                                array_push($array_exclude_post,$post->ID);
                            }
                        }
                    }
                  ?>
              </ul>
            </div>
        </div>
        
        <div class="container" id="pagination_container">
            
            
            
                
        <?php
            $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
            $args = array(
                'order' => 'DESC',
                'cat' => $category_id,
                'post__not_in' => $array_exclude_post,
                'paged' => $paged,
                'post_type' => 'post'
            );
            $the_query = new WP_Query( $args );
            if ( $the_query->have_posts()): ?>
                <h2>ÚLTIMOS ENSAYOS</h2>
                <div class="items">
                <?php while ( $the_query->have_posts()){ $the_query->the_post(); ?>
                    <div class="item row">
                        <div class="three columns">
                            <?php if(has_post_thumbnail()){ ?>
                                <amp-img layout="responsive"
                                  class="image"
                                  width="289"
                                  height="159"
                                  src="<?php echo get_the_post_thumbnail_url($post->ID);?>"</amp-img>
                            <?php }else{?>
                                <amp-img layout="responsive"
                                  class="image"
                                  width="289"
                                  height="159"
                                  src="https://studio-sub.com/clientes/bestiario_wordpress/wp-content/uploads/2020/01/no_thumb.jpg"</amp-img>
                            <?php }?>
                        </div>
                        <div class="nine columns">
                            <p class="categorie_name"><?php the_category($post->ID);?></p>
                            <p class="name_"><?php the_title(); ?></p>
                            <p class="description"><?php the_excerpt();?></p>
                            <a href="<?php echo get_permalink($post->ID);?>"><button class="btn_explorar">EXPLORAR</button></a>
                        </div>
                    </div>
                    <hr class="item_breaker">
                <?php }?>
            <?php else: ?>
                No post
            <?php endif;?>
            <div class="pagination-wrap">
                <?php 
                    $pagination_a = paginate_links( array(
                        'base'         => str_replace( 999999999, '%#%', esc_url( get_pagenum_link( 999999999 ) ) ),
                        'total'        => $the_query->max_num_pages,
                        'current'      => max( 1, get_query_var( 'paged' ) ),
                        'format'       => '?paged=%#%',
                        'show_all'     => false,
                        // 'type'         => 'list',
                        'type'         => 'array',
                        'end_size'     => 1,
                        'mid_size'     => 1,
                        'prev_next'    => true,
                        'prev_text'    => sprintf( '<i></i> %1$s', __( '...', 'text-domain' ) ),
                        'next_text'    => sprintf( '%1$s <i></i>', __( '...', 'text-domain' ) ),
                        'add_args'     => false,
                        'add_fragment' => '',
                    ) );
                    
                    if(!$pagination_a){
                        
                    }else{
                        echo '<ul class="pagination">';
                        foreach ($pagination_a as $value) { 
                            $pos = strpos($value, 'current');
                            if($pos){
                                echo '<li class="active">';
                            }else{
                                echo '<li>';
                            }
                            echo $value;
                            echo '</li>';
                        }
                        echo '</ul>';
                    }
                ?>
            </div>
        <?php     
            /* Restore original Post Data */
            wp_reset_postdata();
        ?>
        
      <div class="" id="container_subir">
        <div class="subir_container">
          <svg width="25" height="60" viewBox="0 0 25 60">
              <use id="" xlink:href="<?php bloginfo('template_directory'); ?>/img/subir.svg#subir_path"></use>
          </svg>
          <p>SUBIR</p>
        </div>
      </div>
      
    </main>

<?php get_footer(); ?>