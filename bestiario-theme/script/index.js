    $(".open_modal").trigger("click");
    //https://stackoverflow.com/questions/544993/official-way-to-ask-jquery-wait-for-all-images-to-load-before-executing-somethin
    $(window).on('load', function () {
        // console.log("load finished");
        $(".close_modal").trigger("click");
    });
    
    
    var bestiario_container = document.getElementsByClassName("bestiario_container_img");
    var i;
    for (i = 0; i < bestiario_container.length; i++) {
      switch(i){
        case 0:bestiario_container[i].classList.add("floating");break;
        case 1:bestiario_container[i].classList.add("floating1");break;
        default:bestiario_container[i].classList.add("floating");
      }
    }
    
    var bestiario_img = document.getElementsByClassName("carousel_container_img");
    for (i = 0; i < bestiario_img.length; i++) {
      switch(i){
        case 0:bestiario_img[i].classList.add("floating");break;
        case 1:bestiario_img[i].classList.add("floating1");break;
        case 2:bestiario_img[i].classList.add("floating2");break;
        case 3:bestiario_img[i].classList.add("floating3");break;
        default:bestiario_img[i].classList.add("floating");
      }
    }
    
    function mapRange (value, a, b, c, d) {
        // first map value from (a..b) to (0..1)
        value = (value - a) / (b - a);
        // then map it from (0..1) to (c..d) and return it
        return c + value * (d - c);
    }
    
    
    //detect hovers
    $(".carousel_container_img").mouseover(function(){
        var id_actual = $(this).attr("id");
        console.log(id_actual);
        var caption_id = "#" + id_actual + "_caption";
        $('.carousel_container_img').each( function(){
          if($(this).attr("id") != id_actual){
            $(this).addClass("inactive");
          }else{
            $(this).addClass("active");
            //show caption
            $(caption_id).addClass("showCaption");
            animateSVG(id_actual);
          }
        }).mouseout(function(){
          $('.carousel_container_img').each( function(){
            $(this).removeClass("inactive");
            $(this).removeClass("active");
            $(".carousel_container_img_caption").removeClass("showCaption");
            $(".carousel_container_img svg use").removeClass();
          });
        });
    });
    
    
    function animateSVG(id_actual){
        var svg_id_actual = "#"+id_actual+" svg use";
        $(svg_id_actual).each(function(index){
            //entrañas
            if($(this).attr("id") == "entranas_circuloRotate"){$(this).addClass("circuloRotate");}
            if($(this).attr("id") == "entranas_tentaculoAni"){$(this).addClass("tentaculoAni");}
            if($(this).attr("id") == "entranas_tentaculoRotate"){$(this).addClass("tentaculoRotate");}
            if($(this).attr("id") == "entranas_circuloScale"){$(this).addClass("circuloScale");}
        
            //bitacora
            if($(this).attr("id") == "bitacora_flauta0"){$(this).addClass("flautaScale0");}
            if($(this).attr("id") == "bitacora_tijeras"){$(this).addClass("tijerasScale");}
            if($(this).attr("id") == "bitacora_flauta1"){$(this).addClass("flautaScale1");}
            if($(this).attr("id") == "bitacora_flauta2"){$(this).addClass("flautaScale2");}
            if($(this).attr("id") == "bitacora_flauta3"){$(this).addClass("flautaScale3");}
            if($(this).attr("id") == "bitacora_manoPluma"){$(this).addClass("manoTransform");}
            if($(this).attr("id") == "bitacora_ala"){$(this).addClass("alaTransform");}
        
        
            //profundidades
            if($(this).attr("id") == "profundidades_pez"){$(this).addClass("pezRotate");}
            if($(this).attr("id") == "profundidades_medusa"){$(this).addClass("medusaScale");}
            if($(this).attr("id") == "profundidades_pulpo2"){$(this).addClass("pulpo2Transform");}
            if($(this).attr("id") == "profundidades_planta2"){$(this).addClass("planta2Transform");}
            if($(this).attr("id") == "profundidades_planta3"){$(this).addClass("planta3Transform");}
        
        }); 
    }
    
    // function checkCarouselHeight(){
    //     // console.log("checkHiehgt");
    //     console.log(window.innerWidth );
    //     var width_size = window.innerWidth ;
    //     if(width_size < 690 ){
    //         // document.getElementsByClassName("carousel").style.height = "35vh";
    //         document.getElementsByClassName("carousel").style.height = "35vh";
    //         //   $(".carousel").css({ "height":"35vh" });
    //     }else{
    //         // document.getElementsByClassName("carousel").style.height = "70vh";
    //         document.getElementsByClassName("carousel").style.height = "70vh";
    //         // $(".carousel").css({"height":"70vh" });
    //     }
    // }
    
    // checkCarouselHeight();
    
    // window.onresize = function(event) {checkCarouselHeight(); };
    