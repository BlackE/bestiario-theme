<?php get_header("alt"); ?>

    <?php
        if(have_posts()):
            $categorie = get_the_category($post->ID);
            $category_id = $categorie[0]->cat_ID;
    ?>
        <?php if(get_post_type() == 'post'): ?>
            <amp-img alt="Categoria_thumb"
                src="
                <?php
                    if (function_exists('ttw_thumbnail_url')){
                        echo ttw_thumbnail_url($category_id);
                    }
                ?>"      
                width="1280"c
                height="400"
                layout="responsive">
        </amp-img>
        <?php endif;?>
        <div id="primary" class="content-area">
        	<main id="main" class="site-main" role="main">
        	    <div class="container">
        	        <div class="eight columns">
        	            <?php
                    		// Start the loop.
                    		while ( have_posts() ) :
                    			the_post();
                    			echo get_template_part( 'templates/content', 'single' );
                    			$post_tags = get_the_tags($postID);
                                if (!empty($post_tags)) {
                                    echo '<ul class="tags_container">';
                                    echo '<p>Etiquetas</p>';
                                    foreach ($post_tags as $tag) {
                                        echo '<li><a href="' . get_tag_link($tag->term_id) . '">' . $tag->name . '</a></li>';
                                    }
                                    echo '</ul>';
                                }
                    			
                    			if ( is_singular( 'attachment' ) ) {
                    				// Parent post navigation.
                    				the_post_navigation(
                    					array(
                    						'prev_text' => _x('<span class="meta-nav">Publicado en</span><span class="post-title">%title</span>', 'Parent post link', 'bestiario-theme' ),
                    					)
                    				);
                    			} elseif ( is_singular( 'post' ) ) {
                    				// Previous/next post navigation.
                    				echo '<div class="row" id="single_paging">';
                                    $prevPost = get_previous_post(true);
                                    if($prevPost) {
                                        echo '<div class="one-half column" id="prev" >';
                                        echo '<p>Anterior</p>';
                                        $prevthumbnail = get_the_post_thumbnail($prevPost->ID, array(100,100) );
                                        previous_post_link('%link',"$prevthumbnail  <p>%title</p>", TRUE);
                                        echo '</div>';
                                         
                                    } $nextPost = get_next_post(true);
                                    if($nextPost) { 
                                        echo '<div class="one-half column" id="next">';
                                        echo '<p>Siguiente</p>';
                                        $nextthumbnail = get_the_post_thumbnail($nextPost->ID, array(100,100) );
                                        next_post_link('%link',"$nextthumbnail  <p>%title</p>", TRUE);
                                        echo '</div>';
                                    }
                    				echo '</div>';
                    			}
                    			// End of the loop.
                    		endwhile;
                    		?>
                    		<div class="related_post">
                    		    <?php
                                    $related = get_posts( array( 'category__in' => wp_get_post_categories($post->ID), 'numberposts' => 4, 'post__not_in' => array($post->ID) ) );
                                    if( $related ) ?>
                                        <h3>También podría interesarte</h3>
                                        <div class="row">
                                        <?php
                                            foreach( $related as $post ):
                                                setup_postdata($post); ?>
                                                <div class="three columns">
                                            
                                                    <a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title(); ?>">
                                                        <?php
                                                        if(has_post_thumbnail()){ 
                                                            $id_thumb = get_post_thumbnail_id($post->ID);
                                                            $attachment = wp_get_attachment_image_src( $id_thumb );
                                                            $width_thumb_related=$attachment[1];
                                                            $height_thumb_related=$attachment[2];
                                                            $url=$attachment[0];
                                                        ?>
                                                            <amp-img layout="responsive"
                                                                class="image"
                                                                width="<?php echo $width_thumb_related;?>"
                                                                height="<?php echo $height_thumb_related;?>"
                                                              src="<?php echo $url?>"</amp-img>
                                                        <?php }else{?>
                                                            <amp-img layout="responsive"
                                                              class="image"
                                                              width="290"
                                                              height="160"
                                                              src="https://studio-sub.com/clientes/bestiario_wordpress/wp-content/uploads/2020/01/no_thumb.jpg"</amp-img>
                                                        <?php }?>
                                                        <p><?php the_title(); ?>
                                                    </a>
                                                </div>
                                            <?php endforeach;?>
                                        </div>   
                                    <?php 
                                        wp_reset_postdata(); 
                                    ?>
                            </div>
                            
                            <?php
                                // If comments are open or we have at least one comment, load up the comment template.
                    			if ( comments_open() || get_comments_number() ) {
                    				comments_template();
                    			}
                            ?>
        	        </div>
        	        <div class="four columns" id="no-responsive">
        	            <?php if ( is_active_sidebar( 'desktop-side-bar' ) ) : ?>
                            <?php dynamic_sidebar( 'desktop-side-bar' ); ?>
                        <?php endif; ?>
        	            <?php 
        	               //get_sidebar(); 
        	            ?>
        	        </div>
        	    </div>
        	</main><!-- .site-main -->
        </div><!-- .content-area -->
        
    <?php endif;?>

    
    <div class="sidebar_button">
        <button id="target-element" on="tap:sidebar1.toggle"><i class="arrow left"></i> </button>
    </div>
    <amp-sidebar id="sidebar1" layout="nodisplay" side="right">
        <?php if ( is_active_sidebar( 'mobile-side-bar' ) ) : ?>
            <?php dynamic_sidebar( 'mobile-side-bar' ); ?>
        <?php endif; ?>
    </amp-sidebar>

<?php get_footer(); ?>
